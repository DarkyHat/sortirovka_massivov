#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include "pch.h"
#include <locale.h>
#include <cstdio>
#include <iostream>
#include <ctime>  //Бибилотека для функции time(используется для создания случайных чисел)

#define Pi 3,14
using namespace std;

int cnt = 0;


void vivod(double *buff, int i, int &n_cmp, int &n_move) { //Функция вывода значений
	cout << "Отсортированный массив" << endl;
	for (cnt = 0; cnt < i; cnt++) {
		cout << buff[cnt] << " ";
	}
	cout << endl << endl;
	cout << "Номер сравнений = " << n_cmp << endl;
	cout << "Номер перемещений = " << n_move << endl << endl;
}


void show_mas(double *buff, int i) { // функция вывода массива до сортировки
	cout << "массив до преобразования" << endl;
	for (cnt = 0; cnt < i; cnt++) {
		cout << buff[cnt] << " ";
	}
	cout << endl << endl;
}


void create_mas(double *buff, int i) {// функция заполнения массива 
	srand(time(0));// автоматическая рандомизация. Если убрать, то  создаваться будут одинаковые числа при одинаковом значении количества чисел. Тут же оргументом служит время, поэтому одинаковые не сгенерируются. Для удобства понимания можно закомменить
	for (cnt = 0; cnt < i; cnt++) { //При повторном запуске программы, печатаются те же самые числа. Суть в том, что функция rand() один раз генерирует  случайные числа, а при последующих запусках программы всего лишь отображает сгенерированные первый раз числа. Такая особенность функции rand() нужна для того, чтобы можно было правильно отладить разрабатываемую программу.
		buff[cnt] = 1 + rand() % 4000  + 1;//заполнение случайными числами от 1 до 4000
	}
	for (cnt = 0; cnt < i; cnt++) {// и вывод на экран
		cout << buff[cnt] << " ";
	}
	cout << endl << endl;
	system("pause");
}


void metod_vibora(double *buff, int i, int &n_cmp, int &n_move) { // 1 способ, метод выбора
	show_mas(buff, i); // показать массив до преобразования
	for (cnt = 0; cnt < i; cnt++){
		int min = cnt;
		for (int t = cnt + 1; t < i; t++){
			if (buff[t] < buff[min]){
				min = t;
			}
			n_cmp++;
		}
		if (min != cnt) {
			int l = buff[cnt];
			buff[cnt] = buff[min];
			buff[min] = l;
			n_move++;
		}
	}
	vivod(buff, i, n_cmp, n_move); //вывод занчений
	system("pause");
}

void metod_vstavok(double *buff, int i, int &n_cmp, int &n_move) { //2 метод вставок.
	show_mas(buff, i);
	for (cnt = 1; cnt < i; cnt++) {
		int l = buff[cnt], s = cnt - 1;
		while (buff[s] < buff[cnt]) {
			n_cmp++;
			s++;
		}
		s = cnt - 1;
		while (s >= 0 && buff[s] > l){
			buff[s+1] = buff[s];
			s--;
			n_cmp++;
			n_move++;
		}
		if (s + 1 != cnt) {
			buff[s + 1] = l;
			n_move++;
			n_cmp++;
		}
	}
	vivod(buff, i, n_cmp, n_move);
	system("pause");
}

void metod_pusirka(double *buff, int i, int &n_cmp, int &n_move) { //3 метод обмена(пузырька)
	show_mas(buff, i);
	int last = 0, con = 0;
	for (cnt = 0; cnt < i; cnt++) {
		for (int con = 0; con < i - 1; con++) {
			if (buff[con] > buff[con + 1]) {
				last = buff[con];
				buff[con] = buff[con + 1];
				buff[con + 1] = last;
				n_move++;
			}
			n_cmp++;
		}
	}
	vivod(buff, i, n_cmp, n_move);
	system("pause");
}




void metod_bistroy_sort(double *buff, int first, int last, int &n_cmp, int &n_move) {//рекурсивный алгоритм хаорта
	int l = first, j = last;
	double tmp, x = buff[(int)((first + last)/2)];

	while (l <= j){
		while (buff[l] < x) {
			l++;
			n_cmp++;
		}
		n_cmp++;
		while (buff[j] > x) {
			j--;
			n_cmp++;
		}
		n_cmp++;
		if (l <= j){
			if (l < j){
				tmp = buff[l];
				buff[l] = buff[j];
				buff[j] = tmp;
				n_move++;
			}
			l++;
			j--;
		}
	} 

	if (l < last) {
		metod_bistroy_sort(buff, l, last, n_cmp, n_move); //рекурсия(вызов функции в самой себе)
	}
	if (first < j) {
		metod_bistroy_sort(buff, first, j, n_cmp, n_move);//рекурсия
	}
	
}

void obratniy_poriadok(double *buff, int i, int &n_cmp, int &n_move) {//сортировка элементов массива в обратном порядке для выполенения задания
	show_mas(buff, i);
	for (cnt = 0; cnt < i; cnt++) {
		int min = cnt;
		for (int t = cnt + 1; t < i; t++) {
			if (buff[t] > buff[min]) {
				min = t;
			}
			n_cmp++;
		}
		if (min != cnt) {
			int l = buff[cnt];
			buff[cnt] = buff[min];
			buff[min] = l;
			n_move++;
		}
	}
	vivod(buff, i, n_cmp, n_move);
	system("pause");
}

int main() {
	setlocale(0, "rus");
	int n_cmp = 0, n_move = 0; // количество сравнений и перемещений
	int i = 0;
	printf("Введите количество элементов массива\n");
	scanf_s("%d", &i);
	double *buff = new double[i];
	create_mas(buff, i); //заполняем массив значениями
	int first = 0, last = i - 1; //аргументы для алгоритма хаорта(4)
	int k = 1;
	while (k != 0) {
		system("cls");
		cout << "Меню" << endl;
		cout << "1 - метод выбора" << endl;
		cout << "2 - метод вставок" << endl;
		cout << "3 - метод пузырька" << endl;
		cout << "4 - быстрая сортировка" << endl;
		cout << "5 - сортировка в обратном порядке" << endl;
		cout << "0 - завершить работу" << endl;
		cin >> k;
		switch (k) {
			case 1:metod_vibora(buff, i, n_cmp, n_move); break;
			case 2:metod_vstavok(buff, i, n_cmp, n_move); break;
			case 3:metod_pusirka(buff, i, n_cmp, n_move); break;
			case 4:show_mas(buff, i);
				metod_bistroy_sort(buff, first, last, n_cmp, n_move);
				vivod(buff, i, n_cmp, n_move);
				system("pause");
				break;
			case 5: obratniy_poriadok(buff, i, n_cmp, n_move); break;
			case 0: exit(1); break;
		}
		n_cmp = 0; //обнуление, чтобы после каждой сортировки можно было смотреть заново
		n_move = 0;// количество сортировок и не перезапускать программу
	}
	return 0;
}